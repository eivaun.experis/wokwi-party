# Wokwi - Task: 5.4 Crazy Party
## "Ode to Party"

There's a submenu for selecting a drink.

You can select a mode for the LED ring through a sub menu. the options are: off, stripe and rainbow.

There's a buzzer playing music which can be enabled/disabled in the menu.There's a LED dot matrix displaying the 4 last notes played of the song.

The menu is navigated with the joystick. Move the joystick up/down to select an item and press the center button or move the joystick to the right to activate. Move the joystick to the left to exit a submenu.


## Screenshot

![screenshot](screenshot.png)


## Contributors

Eivind Vold Aunebakk
