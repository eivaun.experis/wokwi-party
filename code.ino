#include <Servo.h>
#include <LiquidCrystal_I2C.h>
#include <LedControl.h>
#include <Adafruit_NeoPixel.h>

// Pins
#define PIN_VERT A1
#define PIN_HORZ A0

#define PIN_SEL 3

#define PIN_STEP0 12
#define PIN_STEP1 11
#define PIN_STEP2 10

#define PIN_BUZ 2

#define PIN_CS  8
#define PIN_CLK 9
#define PIN_DIN 7
#define MAX_SEG 4

#define PIN_RING 4

// Menu
enum class MenuAction { None, Enter, Back };

enum class Menu {Main, Drinks, PourDrink, Disco };
Menu current_menu = Menu::Main;
int current_item = 0;
String main_menu_items[4] {"Drinks menu", "Disco menu", "Toggle music", ""};
char* music_items[2] {"Enable music ", "Disable music"};
char* drinks_menu_items[4] {"Water", "Juice", "Milk", "DUS"};
char* disco_menu_items[4] {"Off", "Stripe", "Rainbow", ""};

// Music
const String notes = "BBCDDCBAGGABBAABBCDDCBA""GGABAGGAABGABCBGABCBA""GADBBCDDCBAGGABAGG";
const String times = "22222222222223122222222""222223122222222222222""221222222222222231";
const String dot =   "00000000000010000000000""000010000000000000000""000000000000000100";

const int note_freq[7] {220, 246, 261, 293, 329, 349, 196};
const int note_time[5] {16, 8, 4, 2, 1};

const float dot_mul = 1.50f;
const float global_mul = 60;
const int pause = 100;
bool music_enabled = false;

volatile bool sel_pressed = false;
void on_sel_pressed() {
  sel_pressed = true;
}

// LED
#define LED_COUNT 150
Adafruit_NeoPixel ring(LED_COUNT, PIN_RING, NEO_GRB + NEO_KHZ800);

enum LedRingMode { Off, Stripe, Rainbow };
LedRingMode current_ring_mode = LedRingMode::Off;
const int stripe_step = 1;
const int rainbow_step = 4;

LedControl dot_mat = LedControl(PIN_DIN, PIN_CLK, PIN_CS, MAX_SEG);
int dot_mat_levels[4] {7, 7, 7, 7};

// LCD
LiquidCrystal_I2C lcd(0x27, 20, 4);

// Steppers
void step(uint8_t steper, int num_steps)
{
  switch (steper)
  {
    case 0: steper = PIN_STEP0; break;
    case 1: steper = PIN_STEP1; break;
    case 2: steper = PIN_STEP2; break;
  }
  for (int i = 0; i < num_steps; i++)
  {
    digitalWrite(steper, HIGH);
    digitalWrite(steper, LOW);
    delay(5);
  }
}

// Input
int read_axis(uint8_t pin) {
  return map(analogRead(pin), 0, 1023, -1, 1);
}

float clamp(float v, float min_v, float max_v) {
  return max(min_v, min(max_v, v));
}

// Setup
void setup()
{
  Serial.begin(115200);

  pinMode(PIN_VERT, INPUT);
  pinMode(PIN_HORZ, INPUT);
  pinMode(PIN_SEL, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIN_SEL), on_sel_pressed, RISING);

  pinMode(PIN_STEP0, OUTPUT);
  pinMode(PIN_STEP1, OUTPUT);
  pinMode(PIN_STEP2, OUTPUT);

  step(0, 2);
  step(1, 2);
  step(2, 2);

  lcd.init();
  lcd.backlight();

  main_menu_items[2] = music_items[music_enabled];

  dot_mat.shutdown(0, false);
  dot_mat.setIntensity(0, 7);
  dot_mat.clearDisplay(0);

  ring.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  ring.show();            // Turn OFF all pixels ASAP
  ring.setBrightness(255); // Set BRIGHTNESS to about 1/5 (max = 255)

  set_menu(Menu::Main);
}


void loop()
{
  run_menu();
  if (music_enabled) {
    play_music();
  }

  switch (current_ring_mode)
  {
    case LedRingMode::Stripe: ring_stripe(); break;
    case LedRingMode::Rainbow: ring_rainbow(); break;
  }

  static int time = millis();
  int now = millis();
  int dt = now - time;
  time = now;

  int min_dt = 50; 
  delay(max(0, min_dt - dt));
}

MenuAction process_menu_input(int num_items)
{
  int vert = -read_axis(PIN_VERT);
  int horz = -read_axis(PIN_HORZ);

  current_item += vert;
  current_item = clamp(current_item, 0, num_items - 1);

  if (vert != 0)
  {
    for (int i = 0; i < 4; i++)
    {
      lcd.setCursor(0, i);
      lcd.print(current_item == i ? "->" : "  ");
    }
  }

  if (sel_pressed || horz > 0)
  {
    sel_pressed = false;
    return MenuAction::Enter;
  }
  else if (horz < 0)
  {
    return MenuAction::Back;
  }

  return MenuAction::None;
}

void run_menu()
{
  switch (current_menu)
  {
    case Menu::Main: menu_main(); break;
    case Menu::Drinks: menu_drinks(); break;
    case Menu::PourDrink: menu_pour_drink(); break;
    case Menu::Disco: menu_disco(); break;
  }
}

void set_menu(Menu menu)
{
  lcd.clear();
  current_menu = menu;
  current_item = 0;

  for (int i = 0; i < 4; i++)
  {
    lcd.setCursor(2, i);
    switch (current_menu)
    {
      case Menu::Main: lcd.print(main_menu_items[i]); break;
      case Menu::Drinks: lcd.print(drinks_menu_items[i]); break;
      case Menu::Disco: lcd.print(disco_menu_items[i]); break;
    }
  }

  lcd.setCursor(0, 0);
  lcd.print("->");
}

void menu_main()
{
  MenuAction action = process_menu_input(3);

  if (action == MenuAction::Enter)
  {
    switch (current_item)
    {
      case 0: set_menu(Menu::Drinks); break;
      case 1: set_menu(Menu::Disco); break;
      case 2: {
          music_enabled = !music_enabled;
          main_menu_items[2] = music_items[music_enabled];
          set_menu(Menu::Main);
          current_item = 2; // Avoid setting the cursor at the top item
          break;
        }
    }
  }
}

void menu_drinks()
{
  MenuAction action = process_menu_input(4);

  if (action == MenuAction::Back) {
    set_menu(Menu::Main);
  }
  else if (action == MenuAction::Enter)
  {
    pour_drink(current_item);
  }
}

void menu_disco()
{
  MenuAction action = process_menu_input(3);

  if (action == MenuAction::Back) {
    set_menu(Menu::Main);
  }
  else if (action == MenuAction::Enter)
  {
    switch (current_item)
    {
      case 0: set_led_ring_mode(LedRingMode::Off); break;
      case 1: set_led_ring_mode(LedRingMode::Stripe); break;
      case 2: set_led_ring_mode(LedRingMode::Rainbow); break;
    }
  }
}

void menu_pour_drink()
{
  static bool reset = true;
  static long start_time = 0;
  long pour_time = 1000;

  if (reset) {
    reset = false;
    start_time = millis();
  }
  if (millis() >= start_time + pour_time)
  {
    reset = true;
    set_menu(Menu::Drinks);
  }
}

void pour_drink(int i)
{
  set_menu(Menu::PourDrink);

  lcd.setCursor(0, 0);
  lcd.print("Pouring drink: ");
  lcd.print(drinks_menu_items[i]);

  if (i == 3)
  {
    for (int i = 0; i < 10; i++)
    {
      digitalWrite(PIN_STEP0, HIGH);
      digitalWrite(PIN_STEP1, HIGH);
      digitalWrite(PIN_STEP2, HIGH);
      digitalWrite(PIN_STEP0, LOW);
      digitalWrite(PIN_STEP1, LOW);
      digitalWrite(PIN_STEP2, LOW);
      delay(5);
    }
  }
  else
  {
    step(i, 10);
  }
}

void play_music()
{
  static int current_note = 0;
  static long next_note_time = millis();
  long now = millis();

  if (now < next_note_time) return;

  int f_index = notes[current_note] - 'A';
  int freq = note_freq[f_index];
  int time = note_time[times[current_note] - '0'] * global_mul;
  time *= (dot[current_note] - '0') ? dot_mul : 1.0f;

  tone(PIN_BUZ, freq, time);
  next_note_time = now + time + pause;

  for (int n = 3; n > 0; n--)
  {
    for (uint8_t i = dot_mat_levels[n]; i <= dot_mat_levels[n - 1]; i++)
    {
      dot_mat.setRow(n, i, 0);
    }
    for (uint8_t i = dot_mat_levels[n - 1]; i <= dot_mat_levels[n]; i++)
    {
      dot_mat.setRow(n, i, 255);
    }
    dot_mat_levels[n] = dot_mat_levels[n - 1];
  }

  int old_level = dot_mat_levels[0];
  int new_level =  5 - ((f_index == 'G' - 'A') ? -1 : f_index);

  for (uint8_t i = old_level; i <= new_level; i++)
  {
    dot_mat.setRow(0, i, 0);
  }
  for (uint8_t i = new_level; i <= old_level; i++)
  {
    dot_mat.setRow(0, i, 255);
  }

  dot_mat_levels[0] = new_level;

  current_note++;
  current_note %= notes.length();
}

void set_led_ring_mode(int mode)
{
  current_ring_mode = mode;
  if (mode == LedRingMode::Off)
  {
    ring_off();
  }
}

void ring_off()
{
  for (int i = 0; i < ring.numPixels(); i++)
  {
    ring.setPixelColor(i, 0);
  }
  ring.show();
}

void ring_stripe()
{
  static int i = 0;
  static int color = 0;
  static uint32_t colors[3] = {
    ring.Color(255, 0, 0),
    ring.Color(0, 255, 0),
    ring.Color(0, 0, 255)
  };

  for (int n = 0; n < stripe_step; n++) {
    ring.setPixelColor(i + n, colors[color]);
  }
  ring.show();

  i += stripe_step;
  if (i >= ring.numPixels())
  {
    i = 0;
    color = (color + 1) % 3;
  };
}

void ring_rainbow()
{
  static long firstPixelHue = 0;

  for (int i = 0; i < ring.numPixels(); i++) 
  {
    int pixelHue = firstPixelHue + (i * 65536L / ring.numPixels());
    ring.setPixelColor(i, ring.gamma32(ring.ColorHSV(pixelHue)));
  }
  ring.show();

  // Color wheel has a hue range of 65536
  firstPixelHue = firstPixelHue + 256 * rainbow_step;
  if (firstPixelHue >= 65536) {
    firstPixelHue = 0;
  }
}
